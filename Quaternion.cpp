#include "Quaternion.h"
#include <assert.h>
#include <math.h>

const float kmQuaternionDot(const Quaternion* q1, const Quaternion* q2)
{
  // A dot B = B dot A = AtBt + AxBx + AyBy + AzBz

  return (q1->w * q2->w +
      q1->x * q2->x +
      q1->y * q2->y +
      q1->z * q2->z);
}

float Quaternion::dot(const Quaternion* quat){
	return kmQuaternionDot(this, quat);
}

void Quaternion::normalize(){
	kmQuaternionNormalize(this,this);
}

///< Normalizes a quaternion
Quaternion* kmQuaternionNormalize(Quaternion* pOut,	const Quaternion* pIn)
{
	float length = kmQuaternionLength(pIn);
	assert(fabs(length) > std::numeric_limits<T>::epsilon());
	kmQuaternionScale(pOut, pIn, 1.0f / length);

	return pOut;
}

///< Returns the length of the quaternion
const float kmQuaternionLength(const Quaternion* pIn)
{
	return sqrtf(kmQuaternionLengthSq(pIn));
}

///< Returns the length of the quaternion squared (prevents a sqrt)
float kmQuaternionLengthSq(const Quaternion* pIn)
{
	return pIn->x * pIn->x + pIn->y * pIn->y +
						pIn->z * pIn->z + pIn->w * pIn->w;
}

Quaternion* kmQuaternionScale(Quaternion* pOut, const Quaternion* pIn, float s)
{
	pOut->x = pIn->x * s;
	pOut->y = pIn->y * s;
	pOut->z = pIn->z * s;
	pOut->w = pIn->w * s;

	return pOut;
}
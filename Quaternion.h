#ifndef QUATERNION_H
#define QUATERNION_H

struct Quaternion
{
  Quaternion(float _w, float _x, float _y, float _z) :
  w(_w), x(_x), y(_y), z(_z) {};

  float w, x, y, z;

  float dot(const Quaternion* quat);

  void normalize();
};

const float kmQuaternionDot(const Quaternion* q1, const Quaternion* q2);
Quaternion* kmQuaternionNormalize(Quaternion* pOut, const Quaternion* pIn);
const float kmQuaternionLength(const Quaternion* pIn);
float kmQuaternionLengthSq(const Quaternion* pIn);
Quaternion* kmQuaternionScale(Quaternion* pOut, const Quaternion* pIn, float s);

#endif

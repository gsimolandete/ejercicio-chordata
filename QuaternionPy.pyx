# distutils: language = c++

cdef extern from "Quaternion.h":
    cdef cppclass Quaternion:
        Quaternion(float, float, float, float)
        float w,x,y,z
        float dot(const Quaternion* quat)
        void normalize()

#he provado a cambiar el nombre de la clase para ver en que afecta
cdef class QuaternionPy:
    cdef Quaternion *_thisptr

    def __cinit__(self, float _w, float _x, float _y, float _z):
        self._thisptr = new Quaternion(_w, _x, _y, _z)
        if self._thisptr == NULL:
            raise MemoryError()

    def __dealloc__(self):
        if self._thisptr != NULL:
            del self._thisptr

    def dot(self, QuaternionPy quat):
        return self._thisptr.dot(quat._thisptr)

    def normalize(self):
        self._thisptr.normalize()

#Getters

    @property
    def w(self):
        return self._thisptr.w

    @property
    def x(self):
        return self._thisptr.x

    @property
    def y(self):
        return self._thisptr.y

    @property
    def z(self):
        return self._thisptr.z


#Setters

    @w.setter
    def w(self, float _w):
        self._thisptr.w = _w;

    @x.setter
    def x(self, val):
        self._thisptr.x = val;

    @y.setter
    def y(self, float _y): #he provado a hacerlo de esta manera
        self._thisptr.y = _y;

    @z.setter
    def z(self, float _z):
        self._thisptr.z = _z;

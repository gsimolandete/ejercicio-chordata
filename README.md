
Ejercicio pedido por Chordata Tech para el proceso de selección.

Creada la clase Quaternion en C++ y QuaternionPy en Python con el superset Cython. 

Funcionalidades añadidas:
* Getters y setters de los valores propios con los que cuentan los Cuaternión. 
* Producto escalar de dos Cuaterniones
* Normalización de un Cuaternión

Capturas de pantalla de las funciones:

![](/Images/image.png)

![](/Images/image2.png)

![](/Images/image3.png)

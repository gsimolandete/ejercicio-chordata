from distutils.core import setup, Extension
from Cython.Build import cythonize

#he provado con este nombre para ver si afecta en algo a la compilacion y a la ejecución,
#veo que es el nombre del archivo y lo que usas para importar
ext = Extension("QuaternionExt",
                sources=["QuaternionPy.pyx","Quaternion.cpp"],
                language="c++")     

setup(name="QuaternionExt",
      ext_modules=cythonize(ext))
